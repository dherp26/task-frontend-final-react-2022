import React, { useEffect, useState } from "react";
import map from 'assets/img/map.jpg';
import banner from 'assets/img/architect.jpg';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import axios from 'axios';
import { Link, useParams } from "react-router-dom";

const Home = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [dataList, setCategories] = useState([]);
    useEffect(() => {
        setIsLoading(true);
        axios.get('https://api-tdp-2022.vercel.app/api/categories')
            .then(function (response) {
                const { data } = response;
                setCategories(data.data);
                setIsLoading(false);
            }).catch(function (error) {
                //Handle error
                console.log(error);
            });
    }, []);

    useEffect(() => {
        console.log(dataList)
    }, [dataList])

    return (
        <>
            <Navbar />
            <header className="display-container content wide" style={{ maxWidth: '1500px' }} id="home">
                <img className="image" src={banner} alt="Architecture" width={1500} height={800} />
                <div className="display-middle center">
                    <h1 className="xxlarge text-white">
                        <span className="padding black opacity-min">
                            <b>EDTS</b>
                        </span>
                        <span className="hide-small text-light-grey">Mart</span>
                    </h1>
                </div>
            </header>

            {
                isLoading ?
                    <div className="row-padding padding-large" style={{ fontSize: '16px', fontWeight: '800', textAlign: 'center' }}>
                        Loading . . .
                    </div> :
                    <div className="content padding" style={{ maxWidth: '1564px' }}>
                        <div className="container padding-32" id="projects">
                            <h3 className="border-bottom border-light-grey padding-16">Products Category</h3>
                        </div>
                        <div className="row-padding">
                            {dataList.map((data, idx) => {
                                return (
                                    <div className="col l3 m6 margin-bottom" key={idx}>
                                        <a href={`/shop?category=${data.id}`}>
                                            <div className="display-container" style={{ boxShadow: '0 2px 7px #dfdfdf', display: 'flex', alignItems: 'center', justifyContent: 'center', height: '300px', padding: '80px' }}>
                                                <div className="display-topleft black padding">{data.name}</div>
                                                <img src={data.image} alt="House" style={{ maxWidth: '100%', minHeight: '100%' }} />
                                            </div>
                                        </a>
                                    </div>
                                )
                            })}
                        </div>
                        <div className="container padding-32">
                            <img src={map} className="image" alt="maps" style={{ width: '100%' }} />
                        </div>
                    </div>
            }
            <Footer />
        </>
    )
}

export default Home