import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import axios from 'axios';
import React, { useEffect, useState } from "react";
import { Link, useParams, useSearchParams } from "react-router-dom";

// https://www.tutorialspoint.com/localstorage-in-reactjs
// https://stackoverflow.com/questions/47715451/localstorage-item-not-updating-in-axios-headers
// https://www.w3schools.com/jsref/prop_win_localstorage.asp
// https://www.freecodecamp.org/news/how-to-persist-a-logged-in-user-in-react/amp/

const Login = () => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [user, setUser] = useState()

    const handleSubmit = async e => {
        e.preventDefault();
        try {
            const user = { username, password };
            // send the username and password to the server
            const response = await axios.post(
                "https://api-tdp-2022.vercel.app/api/login",
                user
            );

            // set the state of the user
            setUser(response.data)

            // store the user in localStorage
            localStorage.setItem('user', `${username}`)
            localStorage.setItem('token', `"${response.data.data.access_token}"`)
            window.location.reload();
            window.location.href = "/";


            // const loggedInUser = localStorage.getItem("user");
            // console.log(response.data.code)
            // console.log(loggedInUser)

        } catch (err) {
            // console.log()
            alert(err.response.data.message)
        }

    };
    // useEffect(() => {
    //     const loggedInUser = localStorage.getItem("user");
    //     if (loggedInUser) {
    //         const foundUser = JSON.parse(loggedInUser);
    //         setUser(foundUser);
    //     }
    // }, []);

    return (
        <div>
            <Navbar />
            <div className="content padding" style={{ maxWidth: '1564px' }}>
                <div className="container padding-32" id="contact" >
                    <h3 className="border-bottom border-light-grey padding-16">Login</h3>
                    <p>Lets get in touch and talk about your next project.</p>
                    <form onSubmit={handleSubmit}>
                        <input className="input border" type="text" placeholder="Username" required name="username" value={username} onChange={(e) => setUsername(e?.target?.value)} />
                        <input className="input section border" type="password" placeholder="Password" required name="Password" value={password} onChange={(e) => setPassword(e?.target?.value)} />
                        <button className="button black section" type="submit">
                            <i className="fa fa-paper-plane" />
                            Login
                        </button>
                    </form>
                </div >
            </div>

            <Footer />
        </div>
    )
}

export default Login