import Footer from 'components/Footer'
import Navbar from 'components/Navbar'
import React, { useEffect, useState } from "react";
import axios from 'axios';

// https://stackoverflow.com/questions/46155/whats-the-best-way-to-validate-an-email-address-in-javascript
// https://www.tutorialspoint.com/regex-in-reactjs
// https://jasonwatmore.com/post/2021/04/22/react-axios-http-put-request-examples //Put axios example

const Profile = () => {
    const [name, setName] = useState({
        value: '',
        errMsg: null
    });
    const [email, setEmail] = useState({
        value: '',
        errMsg: null
    });
    const [password, setPassword] = useState({
        value: '',
        errMsg: null
    });
    const [passwordType, setPasswordType] = useState({
        value: '',
        errMsg: null
    });
    const [phoneNumber, setPhoneNumber] = useState({
        value: '',
        errMsg: null
    });
    const [isLoading, setIsLoading] = useState(false);
    const loggedInUser = localStorage.getItem("user");

    useEffect(() => {
        axios.get(`https://api-tdp-2022.vercel.app/api/profile/${loggedInUser}`)
            .then(function (response) {
                const { data } = response.data;
                setName({ ...name, value: data.name });
                setEmail({ ...email, value: data.email });
                setPhoneNumber({ ...phoneNumber, value: data.phoneNumber });
            }).catch(function (e) {
            });
    }, [loggedInUser]);

    const handleSubmit = (e) => {
        e.preventDefault();
        if (!validation()) return

        setIsLoading(true);
        axios.put(`https://api-tdp-2022.vercel.app/api/profile/${loggedInUser}`, { name: name.value, email: email.value, phoneNumber: phoneNumber.value, password: password.value })
            .then(function (response) {
                const { data } = response;
                setIsLoading(false);
                alert(response.data.message)
                window.location.reload();
            }).catch(function (error) {
                // handle error
                console.log(error);
            });
    };

    const validation = () => {
        let isValid = true;
        setName({ ...name, errMsg: null })
        setEmail({ ...email, errMsg: null })
        setPhoneNumber({ ...phoneNumber, errMsg: null })
        setPassword({ ...password, errMsg: null })
        setPasswordType({ ...passwordType, errMsg: null })

        if (!name.value) {
            setName({ ...name, errMsg: 'Name must not be empty' })
            isValid = false;
        }
        if (!email.value) {
            setEmail({ ...email, errMsg: 'Email is required' })
            isValid = false;

        } else {
            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            if (!pattern.test(email.value)) {
                setEmail({ ...email, errMsg: 'Email invalid' })
                isValid = false;
            }
        }
        if (!phoneNumber.value) {
            setPhoneNumber({ ...phoneNumber, errMsg: 'Phone number is required' })
            isValid = false;

        } else {
            if (!phoneNumber.value.match(/^[0-9]{12}$/)) {
                setPhoneNumber({ ...phoneNumber, errMsg: 'Phone number invalid' })
                isValid = false;
            }
        }
        if (!password.value) {
            setPassword({ ...password, errMsg: 'Password is required' })
            isValid = false;
        }
        if (!passwordType.value) {
            setPasswordType({ ...passwordType, errMsg: 'Retype Password is required' })
            isValid = false;

        } else if (password.value !== passwordType.value) {
            setPasswordType({ ...passwordType, errMsg: 'Retype Password invalid' })
            isValid = false;
        }
        return isValid
    }
    return (
        <div>
            <Navbar />

            <div className="content padding" style={{ maxWidth: '1564px' }}>
                <div className="container padding-32" id="contact" >
                    <h3 className="border-bottom border-light-grey padding-16">My Profile</h3>
                    <p>Lets get in touch and talk about your next project.</p>
                    <form onSubmit={handleSubmit}>
                        <input className="input section border" type="text" placeholder="Name" name="name" value={name.value} onChange={(e) => setName({ ...name, value: e?.target?.value })} />
                        {name.errMsg && <div style={{ color: '#EF144A' }}>{name.errMsg}</div>}
                        <input className="input section border" type="text" placeholder="Email" name="email" value={email.value} onChange={(e) => setEmail({ ...email, value: e?.target?.value })} />
                        {email.errMsg && <div style={{ color: '#EF144A' }}>{email.errMsg}</div>}
                        <input className="input section border" type="text" placeholder="Phone Number" name="phoneNumber" maxLength={12} value={phoneNumber.value} onChange={(e) => setPhoneNumber({ ...phoneNumber, value: e?.target?.value })} />
                        {phoneNumber.errMsg && <div style={{ color: '#EF144A' }}>{phoneNumber.errMsg}</div>}
                        <input className="input section border" type="password" placeholder="Password" name="password" value={password.value} onChange={(e) => setPassword({ ...password, value: e?.target?.value })} />
                        {password.errMsg && <div style={{ color: '#EF144A' }}>{password.errMsg}</div>}
                        <input className="input section border" type="password" placeholder="Retype Password" name="retypePassword" value={passwordType.value} onChange={(e) => setPasswordType({ ...passwordType, value: e?.target?.value })} />
                        {passwordType.errMsg && <div style={{ color: '#EF144A' }}>{passwordType.errMsg}</div>}
                        <button className="button black section" type="submit">
                            <i className="fa fa-paper-plane" />{isLoading ? "Loading" : "Update"}
                        </button>
                    </form>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default Profile