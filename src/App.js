import 'assets/css/style.css';
import 'assets/css/custom.css';
import { Routes, Route, Navigate } from 'react-router-dom';
import Home from './pages/Home';
import Shop from './pages/Shop';
import ProductCard from 'components/ProductCard';
import Login from 'pages/Login';
import Profile from 'pages/Profile';
import Header from 'components/Navbar';

const App = () => {
    // const RequireAuth = ({ children }) => {
    //     const isLoggedIn = window.localStorage.getItem("token");
    //     return isLoggedIn ? children : <Navigate to="/login" />
    // };

    return (
        <>
            {/* <Header /> */}
            <Routes>
                <Route path='/' element={<Home />} />
                <Route path='/shop' element={<Shop />} />
                {/* <Route path='/profile' element={
                    <RequireAuth>
                        <Profile />
                    </RequireAuth>
                } /> */}
                <Route path='/shop/:productId' element={<ProductCard />} />
                {/* <Route path='/shop/category/:productId' element={<Shop />} /> */}
                <Route path='/profile' element={<Profile />} />
                <Route path='/login' element={<Login />} />
            </Routes>
        </>

    );
}

export default App;
