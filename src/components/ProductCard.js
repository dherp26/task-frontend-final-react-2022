import Navbar from './Navbar';
import Footer from './Footer';
import axios from 'axios';
import React, { useEffect, useState } from "react";

import { Link, useParams } from "react-router-dom";

const ProductCard = ({ productId }) => {
    const [productDetails, setProductDetails] = useState();
    const [count, setCount] = useState(0);
    const [total, setTotal] = useState(0);

    const token = localStorage.getItem("token");

    // const logout = () => {
    //     localStorage.clear()
    //     window.location.reload()
    // }
    // const [isLoading, setIsLoading] = useState(false);
    let params = useParams();

    useEffect(() => {

        // setIsLoading(true);
        axios.get(`https://api-tdp-2022.vercel.app/api/products/${params?.productId}`)
            .then(function (response) {
                const { data } = response;
                setProductDetails(data.data);
                // setIsLoading(false);
            }).catch(function (error) {
                console.log(error);
            });
    }, [productId, count]);

    useEffect(() => {
        console.log(productDetails)
        setTotal(count * productDetails?.price)
    }, [productDetails])

    const finalPrice = productDetails?.discount > 0 ? total - (total * (productDetails?.discount / 100)) : total;

    const handleCount = (e) => {
        let value = parseInt(e.target.value);
        if (value > 0) {
            setCount(count);
        }
        if (value <= productDetails?.stock) {
            setCount(value);
        }
    }
    const handleSubmit = () => {
        if (token) {
            alert("Product added to cart successfully");
            window.location.href = "/";
        } else {
            window.location.href = "/login";
        }
    }
    return (
        <div>
            <Navbar />
            <div className="content padding" style={{ maxWidth: '1564px' }}>
                <div className="container padding-32" id="about">
                    <h3 className="border-bottom border-light-grey padding-16">Product Information</h3>
                </div>
                <div className="row-padding card card-shadow padding-large" style={{ marginTop: '50px' }}>
                    <div className="col l3 m6 margin-bottom">
                        <div className="product-tumb">
                            <img src={productDetails?.image} alt="Product 1" />
                        </div>
                    </div>
                    <div className="col m6 margin-bottom">
                        <h3>{productDetails?.title}</h3>
                        <div style={{ marginBottom: '32px' }}>
                            <span>Category : <strong>{productDetails?.category}</strong></span>
                            <span style={{ marginLeft: '30px' }}>Review : <strong>{productDetails?.rate}</strong></span>
                            <span style={{ marginLeft: '30px' }}>Stock : <strong>{productDetails?.stock}</strong></span>
                            <span style={{ marginLeft: '30px' }}>Discount : <strong>{productDetails?.discount} %</strong></span>
                        </div>
                        <div style={{ fontSize: '2rem', lineHeight: '34px', fontWeight: '800', marginBottom: '32px' }}>
                            Rp. {productDetails?.price}
                        </div>
                        <div style={{ marginBottom: '32px' }}>
                            {productDetails?.description}
                        </div>
                        <div style={{ marginBottom: '32px' }}>
                            <div><strong>Quantity : </strong></div>
                            <input type="number" className="input section border" name="total" placeholder='Quantity' onChange={handleCount} value={count} max={productDetails?.stock} />
                        </div>
                        <div style={{ marginBottom: '32px', fontSize: '2rem', fontWeight: 800 }}>
                            Sub Total : Rp. {finalPrice}
                            {
                                productDetails?.discount > 0 && <span style={{ marginLeft: '30px', fontSize: '18px', textDecoration: 'line-through' }}><strong>{total}</strong></span>
                            }
                        </div>
                        {
                            productDetails?.stock > 0 ?
                                <button className='button light-grey block' disabled={count <= 0} onClick={handleSubmit}>Add to cart</button>
                                : <button className='button light-grey block' disabled={true} onClick={handleSubmit}>Empty Stock</button>
                        }
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default ProductCard