import Navbar from './Navbar';
import Footer from '../components/Footer';
import axios from 'axios';
import React, { useEffect, useState } from "react";

import { Link, useParams } from "react-router-dom";

const CategoryCard = () => {
    const [category, setCategory] = useState();
    const [isLoading, setIsLoading] = useState(false);
    let params = useParams();

    useEffect(() => {
        setIsLoading(true);
        axios.get(`https://api-tdp-2022.vercel.app/api/products?category=${params?.productId}`)
            .then(function (response) {
                const { data } = response;
                setCategory(data.data.categoryDetail);
            }).catch(function (error) {
                // handle error
                console.log(error);
            });
    }, []);

    useEffect(() => {
        console.log(category)
    }, [category])
    return (
        <div>
            <Navbar />
            <div className="content padding" style={{ maxWidth: '1564px' }}>
                <div className="container padding-32" id="about">
                    <h3>
                        All Product
                    </h3>
                </div>
                {
                    isLoading ?
                        <div className="row-padding padding-large" style={{ fontSize: '16px', fontWeight: '800', textAlign: 'center' }}>
                            Loading . . .
                        </div> :
                        <div className="row-padding rm-before-after" style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }}>
                            {/* {dataList && dataList.map((data, idx) => {
                                return (
                                    <div className="product-card" key={idx}>
                                        {data.discount > 0 ? <div className="badge">Discount</div> : <div></div>}
                                        <div className="product-tumb">
                                            <img src={data.image} alt="product1" />
                                        </div>
                                        <div className="product-details">
                                            <span className="product-catagory">{data.category}</span>
                                            <h4>
                                                <a href={`/shop/${data.id}`}>{data.title}</a>
                                            </h4>
                                            <p>{data.description}</p>
                                            <div className="product-bottom-details">
                                                <div className="product-price">Rp. {data.price}</div>
                                                <div className="product-links">
                                                    <a href={`/shop/${data.id}`}>View Detail<i className="fa fa-heart"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })} */}
                        </div>
                }
            </div>
            <Footer />
        </div>
    )
}

export default CategoryCard